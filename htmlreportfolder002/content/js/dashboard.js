/*
   Licensed to the Apache Software Foundation (ASF) under one or more
   contributor license agreements.  See the NOTICE file distributed with
   this work for additional information regarding copyright ownership.
   The ASF licenses this file to You under the Apache License, Version 2.0
   (the "License"); you may not use this file except in compliance with
   the License.  You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
var showControllersOnly = false;
var seriesFilter = "";
var filtersOnlySampleSeries = true;

/*
 * Add header in statistics table to group metrics by category
 * format
 *
 */
function summaryTableHeader(header) {
    var newRow = header.insertRow(-1);
    newRow.className = "tablesorter-no-sort";
    var cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 1;
    cell.innerHTML = "Requests";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 3;
    cell.innerHTML = "Executions";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 7;
    cell.innerHTML = "Response Times (ms)";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 2;
    cell.innerHTML = "Network (KB/sec)";
    newRow.appendChild(cell);
}

/*
 * Populates the table identified by id parameter with the specified data and
 * format
 *
 */
function createTable(table, info, formatter, defaultSorts, seriesIndex, headerCreator) {
    var tableRef = table[0];

    // Create header and populate it with data.titles array
    var header = tableRef.createTHead();

    // Call callback is available
    if(headerCreator) {
        headerCreator(header);
    }

    var newRow = header.insertRow(-1);
    for (var index = 0; index < info.titles.length; index++) {
        var cell = document.createElement('th');
        cell.innerHTML = info.titles[index];
        newRow.appendChild(cell);
    }

    var tBody;

    // Create overall body if defined
    if(info.overall){
        tBody = document.createElement('tbody');
        tBody.className = "tablesorter-no-sort";
        tableRef.appendChild(tBody);
        var newRow = tBody.insertRow(-1);
        var data = info.overall.data;
        for(var index=0;index < data.length; index++){
            var cell = newRow.insertCell(-1);
            cell.innerHTML = formatter ? formatter(index, data[index]): data[index];
        }
    }

    // Create regular body
    tBody = document.createElement('tbody');
    tableRef.appendChild(tBody);

    var regexp;
    if(seriesFilter) {
        regexp = new RegExp(seriesFilter, 'i');
    }
    // Populate body with data.items array
    for(var index=0; index < info.items.length; index++){
        var item = info.items[index];
        if((!regexp || filtersOnlySampleSeries && !info.supportsControllersDiscrimination || regexp.test(item.data[seriesIndex]))
                &&
                (!showControllersOnly || !info.supportsControllersDiscrimination || item.isController)){
            if(item.data.length > 0) {
                var newRow = tBody.insertRow(-1);
                for(var col=0; col < item.data.length; col++){
                    var cell = newRow.insertCell(-1);
                    cell.innerHTML = formatter ? formatter(col, item.data[col]) : item.data[col];
                }
            }
        }
    }

    // Add support of columns sort
    table.tablesorter({sortList : defaultSorts});
}

$(document).ready(function() {

    // Customize table sorter default options
    $.extend( $.tablesorter.defaults, {
        theme: 'blue',
        cssInfoBlock: "tablesorter-no-sort",
        widthFixed: true,
        widgets: ['zebra']
    });

    var data = {"OkPercent": 100.0, "KoPercent": 0.0};
    var dataset = [
        {
            "label" : "KO",
            "data" : data.KoPercent,
            "color" : "#FF6347"
        },
        {
            "label" : "OK",
            "data" : data.OkPercent,
            "color" : "#9ACD32"
        }];
    $.plot($("#flot-requests-summary"), dataset, {
        series : {
            pie : {
                show : true,
                radius : 1,
                label : {
                    show : true,
                    radius : 3 / 4,
                    formatter : function(label, series) {
                        return '<div style="font-size:8pt;text-align:center;padding:2px;color:white;">'
                            + label
                            + '<br/>'
                            + Math.round10(series.percent, -2)
                            + '%</div>';
                    },
                    background : {
                        opacity : 0.5,
                        color : '#000'
                    }
                }
            }
        },
        legend : {
            show : true
        }
    });

    // Creates APDEX table
    createTable($("#apdexTable"), {"supportsControllersDiscrimination": true, "overall": {"data": [0.8, 500, 1500, "Total"], "isController": false}, "titles": ["Apdex", "T (Toleration threshold)", "F (Frustration threshold)", "Label"], "items": [{"data": [1.0, 500, 1500, "42 Login-2"], "isController": false}, {"data": [0.0, 500, 1500, "42 Login-0"], "isController": false}, {"data": [1.0, 500, 1500, "22 /index.php"], "isController": false}, {"data": [1.0, 500, 1500, "42 Login-1"], "isController": false}, {"data": [1.0, 500, 1500, "Open login page"], "isController": true}, {"data": [1.0, 500, 1500, "70 Submit Risk with file attachment"], "isController": false}, {"data": [1.0, 500, 1500, "74 Logout-0"], "isController": false}, {"data": [1.0, 500, 1500, "74 Logout-1"], "isController": false}, {"data": [1.0, 500, 1500, "43 Login"], "isController": false}, {"data": [0.0, 500, 1500, "SubmitRiskWithAttachment"], "isController": true}, {"data": [0.0, 500, 1500, "42 Login"], "isController": false}, {"data": [1.0, 500, 1500, "43 Login-0"], "isController": false}, {"data": [1.0, 500, 1500, "74 Logout"], "isController": false}, {"data": [1.0, 500, 1500, "59 Open risk mgmt menu"], "isController": false}, {"data": [1.0, 500, 1500, "75 Logout"], "isController": false}, {"data": [1.0, 500, 1500, "43 Login-1"], "isController": false}]}, function(index, item){
        switch(index){
            case 0:
                item = item.toFixed(3);
                break;
            case 1:
            case 2:
                item = formatDuration(item);
                break;
        }
        return item;
    }, [[0, 0]], 3);

    // Create statistics table
    createTable($("#statisticsTable"), {"supportsControllersDiscrimination": true, "overall": {"data": ["Total", 168, 0, 0.0, 998.4047619047624, 1, 7077, 6660.699999999999, 6957.4, 7046.64, 5.8780308596620126, 15.708302171897415, 5.735617630593751], "isController": false}, "titles": ["Label", "#Samples", "KO", "Error %", "Average", "Min", "Max", "90th pct", "95th pct", "99th pct", "Throughput", "Received", "Sent"], "items": [{"data": ["42 Login-2", 12, 0, 0.0, 38.50000000000001, 28, 63, 59.100000000000016, 63.0, 63.0, 0.5490985631920929, 1.5530534312940423, 0.25363634803697266], "isController": false}, {"data": ["42 Login-0", 12, 0, 0.0, 6764.249999999999, 6306, 7033, 7012.6, 7033.0, 7033.0, 0.42510982003684283, 0.8111958870624911, 0.2158414849617401], "isController": false}, {"data": ["22 /index.php", 12, 0, 0.0, 28.083333333333332, 20, 84, 67.20000000000006, 84.0, 84.0, 0.5450581395348837, 0.7388092750726745, 0.18856137297874276], "isController": false}, {"data": ["42 Login-1", 12, 0, 0.0, 3.166666666666667, 2, 4, 4.0, 4.0, 4.0, 0.5497274268175364, 0.31727432543863665, 0.25338998579870814], "isController": false}, {"data": ["Open login page", 12, 0, 0.0, 28.083333333333332, 20, 84, 67.20000000000006, 84.0, 84.0, 0.5426426698019354, 0.7355351813330921, 0.1877257478294293], "isController": true}, {"data": ["70 Submit Risk with file attachment", 24, 0, 0.0, 96.83333333333334, 86, 109, 107.5, 109.0, 109.0, 1.0955402382800017, 7.439385099511572, 6.856398639704204], "isController": false}, {"data": ["74 Logout-0", 12, 0, 0.0, 36.166666666666664, 30, 55, 52.900000000000006, 55.0, 55.0, 0.5496267118581963, 0.3059445564054413, 0.2345574932441717], "isController": false}, {"data": ["74 Logout-1", 12, 0, 0.0, 20.666666666666664, 19, 25, 24.700000000000003, 25.0, 25.0, 0.5499793757734085, 0.735677978482057, 0.22271300242907557], "isController": false}, {"data": ["43 Login", 12, 0, 0.0, 34.83333333333333, 30, 49, 46.000000000000014, 49.0, 49.0, 0.5490734385724091, 1.8693430422100206, 0.45416523678792037], "isController": false}, {"data": ["SubmitRiskWithAttachment", 12, 0, 0.0, 7080.416666666667, 6581, 7374, 7351.2, 7374.0, 7374.0, 0.4185267857142857, 11.216810771397181, 4.424401691981724], "isController": true}, {"data": ["42 Login", 24, 0, 0.0, 6825.333333333334, 6338, 7116, 7059.5, 7106.25, 7116.0, 0.8482663556356697, 5.951429295673842, 1.5643362536669847], "isController": false}, {"data": ["43 Login-0", 12, 0, 0.0, 2.583333333333333, 1, 4, 3.700000000000001, 4.0, 4.0, 0.5498029872628974, 0.31678101805186476, 0.22711588243379455], "isController": false}, {"data": ["74 Logout", 24, 0, 0.0, 68.08333333333333, 50, 94, 88.5, 93.0, 94.0, 1.097243176518996, 2.809274705115896, 1.1368450115439126], "isController": false}, {"data": ["59 Open risk mgmt menu", 24, 0, 0.0, 33.75, 27, 50, 45.0, 50.0, 50.0, 1.098247380222395, 7.3692863966045845, 0.5148034594792477], "isController": false}, {"data": ["75 Logout", 12, 0, 0.0, 21.83333333333333, 19, 34, 31.00000000000001, 34.0, 34.0, 0.5502063273727648, 0.7328920220082531, 0.22490937356717103], "isController": false}, {"data": ["43 Login-1", 12, 0, 0.0, 31.916666666666664, 26, 46, 43.000000000000014, 46.0, 46.0, 0.5491739508489314, 1.5532666554162282, 0.22739233902338565], "isController": false}]}, function(index, item){
        switch(index){
            // Errors pct
            case 3:
                item = item.toFixed(2) + '%';
                break;
            // Mean
            case 4:
            // Mean
            case 7:
            // Percentile 1
            case 8:
            // Percentile 2
            case 9:
            // Percentile 3
            case 10:
            // Throughput
            case 11:
            // Kbytes/s
            case 12:
            // Sent Kbytes/s
                item = item.toFixed(2);
                break;
        }
        return item;
    }, [[0, 0]], 0, summaryTableHeader);

    // Create error table
    createTable($("#errorsTable"), {"supportsControllersDiscrimination": false, "titles": ["Type of error", "Number of errors", "% in errors", "% in all samples"], "items": []}, function(index, item){
        switch(index){
            case 2:
            case 3:
                item = item.toFixed(2) + '%';
                break;
        }
        return item;
    }, [[1, 1]]);

        // Create top5 errors by sampler
    createTable($("#top5ErrorsBySamplerTable"), {"supportsControllersDiscrimination": false, "overall": {"data": ["Total", 168, 0, null, null, null, null, null, null, null, null, null, null], "isController": false}, "titles": ["Sample", "#Samples", "#Errors", "Error", "#Errors", "Error", "#Errors", "Error", "#Errors", "Error", "#Errors", "Error", "#Errors"], "items": [{"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}]}, function(index, item){
        return item;
    }, [[0, 0]], 0);

});
