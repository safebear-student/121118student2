/*
   Licensed to the Apache Software Foundation (ASF) under one or more
   contributor license agreements.  See the NOTICE file distributed with
   this work for additional information regarding copyright ownership.
   The ASF licenses this file to You under the Apache License, Version 2.0
   (the "License"); you may not use this file except in compliance with
   the License.  You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
var showControllersOnly = false;
var seriesFilter = "";
var filtersOnlySampleSeries = true;

/*
 * Add header in statistics table to group metrics by category
 * format
 *
 */
function summaryTableHeader(header) {
    var newRow = header.insertRow(-1);
    newRow.className = "tablesorter-no-sort";
    var cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 1;
    cell.innerHTML = "Requests";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 3;
    cell.innerHTML = "Executions";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 7;
    cell.innerHTML = "Response Times (ms)";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 2;
    cell.innerHTML = "Network (KB/sec)";
    newRow.appendChild(cell);
}

/*
 * Populates the table identified by id parameter with the specified data and
 * format
 *
 */
function createTable(table, info, formatter, defaultSorts, seriesIndex, headerCreator) {
    var tableRef = table[0];

    // Create header and populate it with data.titles array
    var header = tableRef.createTHead();

    // Call callback is available
    if(headerCreator) {
        headerCreator(header);
    }

    var newRow = header.insertRow(-1);
    for (var index = 0; index < info.titles.length; index++) {
        var cell = document.createElement('th');
        cell.innerHTML = info.titles[index];
        newRow.appendChild(cell);
    }

    var tBody;

    // Create overall body if defined
    if(info.overall){
        tBody = document.createElement('tbody');
        tBody.className = "tablesorter-no-sort";
        tableRef.appendChild(tBody);
        var newRow = tBody.insertRow(-1);
        var data = info.overall.data;
        for(var index=0;index < data.length; index++){
            var cell = newRow.insertCell(-1);
            cell.innerHTML = formatter ? formatter(index, data[index]): data[index];
        }
    }

    // Create regular body
    tBody = document.createElement('tbody');
    tableRef.appendChild(tBody);

    var regexp;
    if(seriesFilter) {
        regexp = new RegExp(seriesFilter, 'i');
    }
    // Populate body with data.items array
    for(var index=0; index < info.items.length; index++){
        var item = info.items[index];
        if((!regexp || filtersOnlySampleSeries && !info.supportsControllersDiscrimination || regexp.test(item.data[seriesIndex]))
                &&
                (!showControllersOnly || !info.supportsControllersDiscrimination || item.isController)){
            if(item.data.length > 0) {
                var newRow = tBody.insertRow(-1);
                for(var col=0; col < item.data.length; col++){
                    var cell = newRow.insertCell(-1);
                    cell.innerHTML = formatter ? formatter(col, item.data[col]) : item.data[col];
                }
            }
        }
    }

    // Add support of columns sort
    table.tablesorter({sortList : defaultSorts});
}

$(document).ready(function() {

    // Customize table sorter default options
    $.extend( $.tablesorter.defaults, {
        theme: 'blue',
        cssInfoBlock: "tablesorter-no-sort",
        widthFixed: true,
        widgets: ['zebra']
    });

    var data = {"OkPercent": 100.0, "KoPercent": 0.0};
    var dataset = [
        {
            "label" : "KO",
            "data" : data.KoPercent,
            "color" : "#FF6347"
        },
        {
            "label" : "OK",
            "data" : data.OkPercent,
            "color" : "#9ACD32"
        }];
    $.plot($("#flot-requests-summary"), dataset, {
        series : {
            pie : {
                show : true,
                radius : 1,
                label : {
                    show : true,
                    radius : 3 / 4,
                    formatter : function(label, series) {
                        return '<div style="font-size:8pt;text-align:center;padding:2px;color:white;">'
                            + label
                            + '<br/>'
                            + Math.round10(series.percent, -2)
                            + '%</div>';
                    },
                    background : {
                        opacity : 0.5,
                        color : '#000'
                    }
                }
            }
        },
        legend : {
            show : true
        }
    });

    // Creates APDEX table
    createTable($("#apdexTable"), {"supportsControllersDiscrimination": true, "overall": {"data": [0.8, 500, 1500, "Total"], "isController": false}, "titles": ["Apdex", "T (Toleration threshold)", "F (Frustration threshold)", "Label"], "items": [{"data": [1.0, 500, 1500, "42 Login-2"], "isController": false}, {"data": [0.0, 500, 1500, "42 Login-0"], "isController": false}, {"data": [1.0, 500, 1500, "22 /index.php"], "isController": false}, {"data": [1.0, 500, 1500, "42 Login-1"], "isController": false}, {"data": [1.0, 500, 1500, "Open login page"], "isController": true}, {"data": [1.0, 500, 1500, "70 Submit Risk with file attachment"], "isController": false}, {"data": [1.0, 500, 1500, "74 Logout-0"], "isController": false}, {"data": [1.0, 500, 1500, "74 Logout-1"], "isController": false}, {"data": [1.0, 500, 1500, "43 Login"], "isController": false}, {"data": [0.0, 500, 1500, "SubmitRiskWithAttachment"], "isController": true}, {"data": [0.0, 500, 1500, "42 Login"], "isController": false}, {"data": [1.0, 500, 1500, "43 Login-0"], "isController": false}, {"data": [1.0, 500, 1500, "74 Logout"], "isController": false}, {"data": [1.0, 500, 1500, "59 Open risk mgmt menu"], "isController": false}, {"data": [1.0, 500, 1500, "75 Logout"], "isController": false}, {"data": [1.0, 500, 1500, "43 Login-1"], "isController": false}]}, function(index, item){
        switch(index){
            case 0:
                item = item.toFixed(3);
                break;
            case 1:
            case 2:
                item = formatDuration(item);
                break;
        }
        return item;
    }, [[0, 0]], 3);

    // Create statistics table
    createTable($("#statisticsTable"), {"supportsControllersDiscrimination": true, "overall": {"data": ["Total", 168, 0, 0.0, 989.2797619047628, 1, 7077, 6421.999999999997, 6962.65, 7071.48, 5.8661266105660115, 15.668271685900345, 5.735186284437306], "isController": false}, "titles": ["Label", "#Samples", "KO", "Error %", "Average", "Min", "Max", "90th pct", "95th pct", "99th pct", "Throughput", "Received", "Sent"], "items": [{"data": ["42 Login-2", 12, 0, 0.0, 37.66666666666666, 29, 59, 56.000000000000014, 59.0, 59.0, 0.5391562205148942, 1.5240991598148896, 0.2490438401401806], "isController": false}, {"data": ["42 Login-0", 12, 0, 0.0, 6700.25, 6047, 7031, 7029.2, 7031.0, 7031.0, 0.42434315216238194, 0.8097329290286078, 0.21479609427490365], "isController": false}, {"data": ["22 /index.php", 12, 0, 0.0, 28.583333333333332, 20, 81, 67.20000000000005, 81.0, 81.0, 0.5412475756619006, 0.733644174822967, 0.1864062288575166], "isController": false}, {"data": ["42 Login-1", 12, 0, 0.0, 3.4166666666666665, 2, 6, 5.400000000000002, 6.0, 6.0, 0.5397625044980209, 0.3115230860921195, 0.2487967794170565], "isController": false}, {"data": ["Open login page", 12, 0, 0.0, 28.583333333333332, 20, 81, 67.20000000000005, 81.0, 81.0, 0.5389624971929037, 0.7305468223669436, 0.1856192454525039], "isController": true}, {"data": ["70 Submit Risk with file attachment", 24, 0, 0.0, 98.91666666666667, 86, 125, 120.5, 125.0, 125.0, 1.0754615522495072, 7.301988343565156, 6.761194322459223], "isController": false}, {"data": ["74 Logout-0", 12, 0, 0.0, 35.5, 30, 56, 54.50000000000001, 56.0, 56.0, 0.5397382269599245, 0.30044022399136416, 0.23033750506004588], "isController": false}, {"data": ["74 Logout-1", 12, 0, 0.0, 20.500000000000004, 18, 23, 22.700000000000003, 23.0, 23.0, 0.5400297016335898, 0.7193364385041178, 0.21991443904414743], "isController": false}, {"data": ["43 Login", 12, 0, 0.0, 34.666666666666664, 27, 53, 50.000000000000014, 53.0, 53.0, 0.5389867049946101, 1.8341689498742364, 0.44582201086956524], "isController": false}, {"data": ["SubmitRiskWithAttachment", 12, 0, 0.0, 7018.25, 6414, 7373, 7367.9, 7373.0, 7373.0, 0.41779820346772506, 11.19272818962816, 4.42754574019915], "isController": true}, {"data": ["42 Login", 24, 0, 0.0, 6760.333333333333, 6084, 7130, 7097.5, 7123.25, 7130.0, 0.8467400508044031, 5.938756879762913, 1.5602122583262772], "isController": false}, {"data": ["43 Login-0", 12, 0, 0.0, 2.166666666666667, 1, 4, 3.700000000000001, 4.0, 4.0, 0.5397139516056491, 0.310967999460286, 0.22294824368084915], "isController": false}, {"data": ["74 Logout", 24, 0, 0.0, 67.79166666666666, 48, 104, 91.5, 103.5, 104.0, 1.0773443461866499, 2.7522781343987073, 1.1178499685774566], "isController": false}, {"data": ["59 Open risk mgmt menu", 24, 0, 0.0, 34.24999999999999, 28, 41, 40.5, 41.0, 41.0, 1.0779734099892202, 7.231931312881782, 0.505300035932447], "isController": false}, {"data": ["75 Logout", 12, 0, 0.0, 22.08333333333334, 18, 30, 28.500000000000007, 30.0, 30.0, 0.54005400540054, 0.7193688118811882, 0.21992433618361837], "isController": false}, {"data": ["43 Login-1", 12, 0, 0.0, 32.16666666666666, 25, 51, 47.40000000000001, 51.0, 51.0, 0.5390351271224508, 1.5237568502380738, 0.2231942323241398], "isController": false}]}, function(index, item){
        switch(index){
            // Errors pct
            case 3:
                item = item.toFixed(2) + '%';
                break;
            // Mean
            case 4:
            // Mean
            case 7:
            // Percentile 1
            case 8:
            // Percentile 2
            case 9:
            // Percentile 3
            case 10:
            // Throughput
            case 11:
            // Kbytes/s
            case 12:
            // Sent Kbytes/s
                item = item.toFixed(2);
                break;
        }
        return item;
    }, [[0, 0]], 0, summaryTableHeader);

    // Create error table
    createTable($("#errorsTable"), {"supportsControllersDiscrimination": false, "titles": ["Type of error", "Number of errors", "% in errors", "% in all samples"], "items": []}, function(index, item){
        switch(index){
            case 2:
            case 3:
                item = item.toFixed(2) + '%';
                break;
        }
        return item;
    }, [[1, 1]]);

        // Create top5 errors by sampler
    createTable($("#top5ErrorsBySamplerTable"), {"supportsControllersDiscrimination": false, "overall": {"data": ["Total", 168, 0, null, null, null, null, null, null, null, null, null, null], "isController": false}, "titles": ["Sample", "#Samples", "#Errors", "Error", "#Errors", "Error", "#Errors", "Error", "#Errors", "Error", "#Errors", "Error", "#Errors"], "items": [{"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}]}, function(index, item){
        return item;
    }, [[0, 0]], 0);

});
